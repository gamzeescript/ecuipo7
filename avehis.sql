drop database if exists avehis;
create database if not exists avehis;
use avehis;

-- Tablas de relación fuerte 

-- Tabla Pais
create table pais(
id_pais int(11) not null primary key auto_increment,
pais varchar(20) not null
)Engine InnoDB;

-- Tabla Rol
create table rol(
id_rol int(11) not null primary key auto_increment,
rol varchar(20) not null
)Engine InnoDB;

-- Tabla Marca
create table marca(
id_marca int(11) not null primary key auto_increment,
marca varchar(20) not null
)Engine InnoDB;

-- Tabla Estado
create table estado(
id_estado int(11) not null primary key auto_increment,
estado varchar(20) not null
)Engine InnoDB;

-- Table Cliente
create table cliente(
id_cliente int(11) not null primary key auto_increment,
nombre varchar(120) not null,
nit varchar(25) not null
)Engine InnoDB; 

-- Tabla Tipo pago
create table tipo_pago(
id_tipo_pago int(11) not null primary key auto_increment,
tipo_pago varchar(20) not null
);





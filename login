<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <title>JSP Page</title>
        <style>
            body{background-image:url("img/background.jpg");}
        </style>
    </head>
    <body>
        <h1 style="text-align: center;">Iniciar sesion</h1>
        <div class="container mt-4 col-lg-4" >
            <div class="card col-sm-10">
                <div class="card-body">
                    <form action="login?action=login" method="POST">
                        <div class="form-group text-center">
                            <label>Login</label><br>
                            <img src="img/login.png" alt="80" width="180"/><br>
                        <label> Usuario:</label>
                        <input type="text" class="form-control" name="nombre_usuario"/><br/><br/>  
                        </div>
                        <div class="form-group text-center">
                        <label>Password:</label>
                        <input type="password" class="form-control form-control-sm" name="clave"/><br/><br/> 
                        </div>
                        <input type="submit" class="btn btn-primary" name="action" value="Ingresar">
                        ${msg} 
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
